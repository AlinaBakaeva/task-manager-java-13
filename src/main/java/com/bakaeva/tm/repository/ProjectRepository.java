package com.bakaeva.tm.repository;

import com.bakaeva.tm.api.repository.IProjectRepository;
import com.bakaeva.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {
    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(final Project project) {
        projects.add(project);
    }

    @Override
    public void remove(final Project project) {
        projects.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public Project findById(final String id) {
        for (final Project project : projects) {
            if (id.equals((project.getId()))) return project;
        }
        return null;
    }

    @Override
    public Project findByIndex(final Integer index) {
        return projects.get(index);
    }

    @Override
    public Project findByName(final String name) {
        for (final Project project : projects) {
            if (name.equals((project.getName()))) return project;
        }
        return null;
    }

    @Override
    public Project removeById(final String id) {
        Project project = findById(id);
        if (project == null) return null;
        remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final Integer index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        remove(project);
        return project;
    }

    @Override
    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        remove(project);
        return project;
    }

}