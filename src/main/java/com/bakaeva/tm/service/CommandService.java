package com.bakaeva.tm.service;

import com.bakaeva.tm.api.repository.ICommandRepository;
import com.bakaeva.tm.api.service.ICommandService;
import com.bakaeva.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @Override
    public String[] getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public String[] getArguments() {
        return commandRepository.getArgs();
    }
}